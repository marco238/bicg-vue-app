module.exports = {
  publicPath: process.env.NODE_ENV === 'production'
    ? '/bicg-vue-app/'
    : '/',
};
