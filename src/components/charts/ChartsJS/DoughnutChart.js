import { Doughnut } from 'vue-chartjs';

export default ({
  extends: Doughnut,
  mounted() {
    this.renderChart({
      type: 'doughnut',
      labels: [
        'Leer docu',
        'Escribir código',
        'Jugar con los estilos',
      ],
      datasets: [
        {
          label: 'Mis ingresos',
          borderColor: '#fff',
          data: [45, 15, 40],
          backgroundColor: ['#ff6384', '#36a2eb', '#4bc0c0'],
          lineTension: 0,
        },
      ],
      option: {
        title: {
          display: true,
          position: 'bottom',
          text: 'Fruits',
        },
      },
    }, { responsive: true, maintainAspectRatio: false });
  },
});
