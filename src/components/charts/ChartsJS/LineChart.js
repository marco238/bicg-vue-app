import { Line } from 'vue-chartjs';

export default ({
  extends: Line,
  mounted() {
    this.renderChart({
      labels: [
        'Abril',
        'Mayo',
        'Junio',
        'Julio',
        'Agosto',
        'Septiembre',
        'Octubre',
        'Noviembre',
        'Diciembre',
        'Enero',
        'Febrero',
        'Marzo',
      ],
      datasets: [
        {
          label: 'Mis ingresos',
          backgroundColor: 'transparent',
          borderColor: 'red',
          data: [0, 0, 0, 750, 750, 750, 750, 1050, 1666, 1666, 1760, 2080],
          lineTension: 0.2,
        },
      ],
    }, { responsive: true, maintainAspectRatio: false });
  },
});
