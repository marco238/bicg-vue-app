import { Bar } from 'vue-chartjs';

export default ({
  extends: Bar,
  mounted() {
    this.renderChart({
      labels: [
        'Julio',
        'Agosto',
        'Septiembre',
        'Octubre',
        'Noviembre',
        'Diciembre',
        'Enero',
        'Febrero',
        'Marzo',
      ],
      datasets: [
        {
          label: 'Mis facturas de Orange',
          backgroundColor: '#ff9800',
          borderColor: '#333',
          data: [120, 118, 134, 55, 57, 60, 60, 62, 0],
          lineTension: 0.2,
        },
      ],
    }, { responsive: true, maintainAspectRatio: false });
  },
});
